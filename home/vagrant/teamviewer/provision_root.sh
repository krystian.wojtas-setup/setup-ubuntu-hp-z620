#!/bin/bash

apt-get update

# install desktop environment
apt-get install -y xfce4

# configure VirtualBox integration
VBoxClient --clipboard
VBoxClient --draganddrop
VBoxClient --display
VBoxClient --checkhostversion
VBoxClient --seamless

# TeamViewer
apt-get update
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
dpkg -i teamviewer_amd64.deb
apt-get install -y -f
